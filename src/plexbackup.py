#!/usr/bin/python3

from zipfile import ZipFile, ZIP_DEFLATED

VERSION = '0.1.20170206'

from platform import system
from sys import argv, exit, stdout
from os import listdir, path, remove
from datetime import datetime

PLEX_DIR = '/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/'
BACKUP_DIR = '/media/cloud/backup/plex/'

SIZE = 0

KEEP_X_LAST_BACKUPS = 2

EXCLUDED = [ 'Cache', 'Crash Reports', 'Logs' ]

def backup(obj, archive, location = ''):

    global SIZE

    try:
        rel = path.join(location, obj)
        if path.isdir(rel):
            if obj in EXCLUDED:
                return
            for f in listdir(rel):
                backup(f, archive, rel)
        else:
            SIZE += path.getsize(rel)
            print('[PBM] Creating a new backup file: %s' % readable(SIZE), end = '\r')
            stdout.write("\033[F")
            stdout.write("\033[K")
            #stdout.flush()
            archive.write(rel)
    except PermissionError as e:
        exit('[=!=] You have to run this script as root or make the Plex Folder accessable for the current user!')
    except Exception as e:
        exit('[PBM] raised an exception: ' + e)

def readable(bytes):

    for unit in [ 'B', 'KiB', 'MiB', 'GiB' ]:
        if abs(bytes) < 1024.0:
            return '%3.1f %s' % (bytes, unit)
        bytes /= 1024.0

    return '%3.1f %s' % (bytes, 'TiB')

def main():

    print('[PBM] Welcome to the Plex Backup Manager!')
    print('[PBM] Build: %s' % VERSION)

    global BACKUP_DIR, EXCLUDED

    try:
        for i, arg in enumerate(argv):
            if arg == '-v' or argv == '--version':
                exit('Plex Server BACKUP v.%s' % VERSION)
            elif arg == '-p' or arg == '--path':
                BACKUP_DIR = argv[i + 1]
            else:
                continue
    except Exception as e:
        exit('Usage: python3 %s [-v | --version] [-p | --path PATH]' % (argv[0]))

    if len(BACKUP_DIR) <= 0 or not path.exists(BACKUP_DIR):
        exit('Usage: python3 %s [-v | --version] [-p | --path PATH]' % (argv[0]))

    backups = sorted([b for b in listdir(BACKUP_DIR) if b.endswith('.zip')])

    print('[BPM] Directory: %s' % BACKUP_DIR)
    print('[BPM] Keep: Last %d dump(s)' % KEEP_X_LAST_BACKUPS)
    print('[BPM] Found: %d backup file(s)' % len(backups))

    del backups[-KEEP_X_LAST_BACKUPS + 1 : ]

    for f in backups:

        print('[PBM] Removing the old backup %s' % f)

        try:
            remove(path.join(BACKUP_DIR, f))
        except Exception as e:
            exit('[PBM] An error occurred while removing %s' % f)

    archive = ZipFile(BACKUP_DIR + 'plexdb-%s.zip' % datetime.now().strftime('%Y-%m-%d-%H%M'), 'w', ZIP_DEFLATED)

    try:
        backup(PLEX_DIR, archive)
    finally:
        archive.close()
        print('[PBM] Git: www.bitbucket.org/schepsen/plex-backup-manager')

    print('[PBM] The JOB was completed successfully!')

if __name__ == '__main__':

    main()
