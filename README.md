
## PROJECT ##

* ID: **P**lex **B**ackup!**M**ANAGER
* Contact: nikolaj@schepsen.eu

## USAGE ##

Usage: python3 plexbackup.py [-v | --version] [-p | --path PATH]

You have to run this script as root or make the Plex Folder accessable for the current user

## REQUIREMENTS ##

* Installed Python 3.0 or higher

## CHANGELOG ##

### Plex Backup!MANAGER, updated @ 2017-02-06 ###

* Added a function keeps X last dump files

### Plex Backup!MANAGER, updated @ 2016-12-29 ###

* Small Fixes

### Plex Backup!MANAGER, updated @ 2016-12-27 ###

* Initial Release
